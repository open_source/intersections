cmake_minimum_required(VERSION 3.15)

project(intersections LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

find_package(lanelet2_core REQUIRED)
find_package(OpenGL REQUIRED)
find_package(GLUT REQUIRED)
find_package(Armadillo 9.9 REQUIRED)

include_directories(include)

add_executable(${CMAKE_PROJECT_NAME}
    src/main.cpp
    src/intersection_extraction.cpp
    src/plot.cpp)

target_link_libraries(${CMAKE_PROJECT_NAME}
    armadillo
    lanelet2_core
    lanelet2_traffic_rules
    lanelet2_io
    lanelet2_projection
    lanelet2_routing
    glut
    GLU
    OpenGL)
