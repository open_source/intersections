#pragma once

#include <lanelet2_core/LaneletMap.h>

struct ConnectionCorridor {
  int64_t id;
  lanelet::ConstLanelets lanelets;
};

struct Intersection {
  int id;
  std::map<int, lanelet::ConstLanelet> lanelet_map;
  std::vector<lanelet::BasicPoint2d> entrances;
  std::vector<lanelet::BasicPoint2d> exits;
  std::vector<lanelet::BasicPolygon2d> intersection_polygons;
  lanelet::BoundingBox2d bounding_box;
  std::vector<ConnectionCorridor> connection_corridors;
  std::unordered_map<int, std::unordered_map<int, int>> priorities;
};


