#pragma once

#include <lanelet2_core/LaneletMap.h>
#include <lanelet2_core/geometry/Polygon.h>
#include <lanelet2_core/primitives/BasicRegulatoryElements.h>
#include <lanelet2_routing/RoutingGraph.h>
#include <boost/geometry.hpp>
#include "intersection.h"

namespace IntersectionExtraction {

std::vector<Intersection> FindIntersections(
    const lanelet::LaneletMapPtr lanelet_map,
    const lanelet::routing::RoutingGraphPtr& lanelet_routing_graph);

void GroupLaneletsRecursive(
    int index, std::map<int, lanelet::ConstLanelet>& interesting_lanelets,
    std::map<int, lanelet::ConstLanelet>& intersection,
    std::vector<std::vector<bool>>& intersection_table);

void SearchSuccesorRecursive(
    int& index,
    std::unordered_map<int, lanelet::ConstLanelets>& bifurcating_lanelets,
    std::unordered_map<int, bool>& exits_found,
    lanelet::ConstLanelet& current_lanelet,
    lanelet::ConstLanelet& previous_lanelet, Intersection& intersection,
    const lanelet::routing::RoutingGraphPtr& lanelet_routing_graph);

bool CorridorsOverlap(
    const ConnectionCorridor& corridor,
    const ConnectionCorridor& other_corridor,
    const lanelet::routing::RoutingGraphPtr& lanelet_routing_graph);

float SignedArea(const lanelet::LaneletSequence& lanelet_sequence);

std::vector<int64_t> GetCorridorsWithLanelet(
    lanelet::ConstLanelet& target_lanelet,
    const std::vector<ConnectionCorridor>& connection_corridors,
    const lanelet::routing::RoutingGraphPtr& lanelet_routing_graph);

}  // namespace IntersectionExtraction
