#pragma once

#include <GL/glut.h>

#include <GL/freeglut_ext.h>
#include <lanelet2_core/LaneletMap.h>
#include <armadillo>
#include "intersection.h"

#define SIZE_X 1366
#define SIZE_Y 768

class Plotter {
 public:
  Plotter(int argc, char** argv);

  void initPlot();

  void updateMap(lanelet::LaneletMapConstPtr map);

  void updateIntersections(const std::vector<Intersection>& intersections);

 private:
  static Plotter* selfPtr_;

  static void display(void);
  static void reshape(int w, int h);
  static void updateAnim(int nothing);
  static void keyboardCB(unsigned char key, int x, int y);
  void plotLanelet(const lanelet::ConstLanelet& lanelet);
  void plotCar(void);
  void plotMap(void);
  void plotIntersections(void);
  void plotPolygon(const lanelet::BasicPolygon2d& polygon, float* color,
                   float line_width, float z);
  void plotBoundingBox(const lanelet::BoundingBox2d bounding_box, float* color);

  lanelet::LaneletMapConstPtr lanelet_map_;
  std::vector<Intersection> intersections_;
  std::vector<float*> colors_;
};
