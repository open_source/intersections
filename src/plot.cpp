#include "plot.h"

Plotter* Plotter::selfPtr_ = nullptr;

static bool mouseRightDown;
static float mouseX;
static float mouseY;
static float cameraDistance = 100;
static float cameraX;
static float cameraY;

void setCamera(float posX, float posY, float posZ, float targetX, float targetY,
               float targetZ, float upX, float upY, float upZ);
void setCamera();
void mouseCB(int button, int stat, int x, int y);
void mouseMotionCB(int x, int y);

Plotter::Plotter(int argc, char** argv) {
  glutInit(&argc, argv);

  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH | GLUT_STENCIL);

  glutInitWindowSize(SIZE_X, SIZE_Y);
  glutInitWindowPosition(0, 0);
  glutCreateWindow("Intersections");

  selfPtr_ = this;

  glutKeyboardFunc(keyboardCB);
  glutMouseFunc(mouseCB);
  glutMotionFunc(mouseMotionCB);

  colors_.resize(8);
  colors_[0] = new float[3]{0, 1, 0};
  colors_[1] = new float[3]{1, 1, 0};
  colors_[2] = new float[3]{0, 0, 1};
  colors_[3] = new float[3]{0, 0.9, 0.9};
  colors_[4] = new float[3]{1, 0, 0};
  colors_[5] = new float[3]{0.5, 0.5, 0.5};
  colors_[6] = new float[3]{1, 0, 1};
  colors_[7] = new float[3]{0, 0, 0.5};
}

void Plotter::initPlot() {
  glutDisplayFunc(display);
  glutReshapeFunc(reshape);
  glutTimerFunc(0, updateAnim, 0);

  glShadeModel(GL_SMOOTH);
  glPixelStorei(GL_UNPACK_ALIGNMENT, 4);

  glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

  glEnable(GL_DEPTH_TEST);
  glEnable(GL_TEXTURE_2D);
  glEnable(GL_CULL_FACE);

  glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
  glEnable(GL_COLOR_MATERIAL);

  glClearColor(1, 1, 1, 0);
  glClearStencil(0);
  glClearDepth(1.0);
  glDepthFunc(GL_LEQUAL);

  setCamera(0, 0, cameraDistance, 0, 0, 0, 0, 1, 0);

  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

void Plotter::reshape(int w, int h) {
  glViewport(0, 0, static_cast<GLsizei>(w), static_cast<GLsizei>(h));
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluPerspective(60.0, static_cast<double>(w) / h, 1.0, 1000.0);

  glMatrixMode(GL_MODELVIEW);
}

void Plotter::updateAnim(int _nothing) {
  glutTimerFunc(1000 / 60, updateAnim, 0);
  glutPostRedisplay();
}

void Plotter::display(void) {
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
  glPushMatrix();
  glColor3f(1.0, 1.0, 1.0);
  glLineWidth(2);
  selfPtr_->plotMap();
  glLineWidth(1);

  selfPtr_->plotIntersections();

  glPopMatrix();
  glutSwapBuffers();
}

void Plotter::plotLanelet(const lanelet::ConstLanelet& lanelet) {
  const lanelet::ConstLineString3d& left_bound = lanelet.leftBound();
  for (std::size_t seg = 0; seg < left_bound.numSegments(); seg++) {
    glBegin(GL_LINES);
    if (!left_bound.empty()) {
      glVertex2d(left_bound.segment(seg).first.x(),
                 left_bound.segment(seg).first.y());
      glVertex2d(left_bound.segment(seg).second.x(),
                 left_bound.segment(seg).second.y());
    }
    glEnd();
  }

  const lanelet::ConstLineString3d& right_bound = lanelet.rightBound();
  for (std::size_t seg = 0; seg < right_bound.numSegments(); seg++) {
    glBegin(GL_LINES);
    if (!right_bound.empty()) {
      glVertex2d(right_bound.segment(seg).first.x(),
                 right_bound.segment(seg).first.y());
      glVertex2d(right_bound.segment(seg).second.x(),
                 right_bound.segment(seg).second.y());
    }
    glEnd();
  }
}

void Plotter::plotMap(void) {
  glColor3f(0.0, 0.0, 0.0);
  for (auto it = lanelet_map_->laneletLayer.begin();
       it != lanelet_map_->laneletLayer.end(); it++) {
    plotLanelet(*it);
  }
}

void Plotter::plotIntersections() {
  for (auto& intersection : intersections_) {
    float* color = colors_[6];
    for (auto& lanelet : intersection.lanelet_map) {
      glColor3f(color[0], color[1], color[2]);
      selfPtr_->plotLanelet(lanelet.second);
    }
    for (auto& entrance : intersection.entrances) {
      glBegin(GL_POINTS);
      glColor3f(0.0, 0.0, 1.0);
      glVertex2f(entrance.x(), entrance.y());
      glEnd();
    }
    for (auto& exit : intersection.exits) {
      glBegin(GL_POINTS);
      glColor3f(1.0, 0.0, 0.0);
      glVertex2f(exit.x(), exit.y());
      glEnd();
    }

    for (const auto& polygon : intersection.intersection_polygons) {
      plotPolygon(polygon, colors_[5], 5, 0);
    }

    plotBoundingBox(intersection.bounding_box, colors_[5]);
  }
}

void Plotter::plotPolygon(const lanelet::BasicPolygon2d& polygon, float* color,
                          float line_width, float z) {
  glColor4f(color[0], color[1], color[2], 0.7f);

  for (size_t i = 0; i < polygon.size() - 1; i++) {
    glPointSize(line_width);
    glBegin(GL_POINTS);
    if (!polygon.empty()) {
      glVertex3f(polygon.at(i).x(), polygon.at(i).y(), z);
      glVertex3f(polygon.at(i + 1).x(), polygon.at(i + 1).y(), z);
    }

    glEnd();
  }
}

void Plotter::plotBoundingBox(const lanelet::BoundingBox2d bounding_box,
                              float* color) {
  glColor4f(color[0], color[1], color[2], 0.7f);
  auto top_left_corner = bounding_box.corner(lanelet::BoundingBox2d::TopLeft);
  auto top_right_corner = bounding_box.corner(lanelet::BoundingBox2d::TopRight);
  auto bottom_right_corner =
      bounding_box.corner(lanelet::BoundingBox2d::BottomRight);
  auto bottom_left_corner =
      bounding_box.corner(lanelet::BoundingBox2d::BottomLeft);

  glBegin(GL_LINES);
  glVertex2d(top_left_corner.x(), top_left_corner.y());
  glVertex2d(top_right_corner.x(), top_right_corner.y());
  glEnd();

  glBegin(GL_LINES);
  glVertex2d(top_right_corner.x(), top_right_corner.y());
  glVertex2d(bottom_right_corner.x(), bottom_right_corner.y());
  glEnd();

  glBegin(GL_LINES);
  glVertex2d(bottom_right_corner.x(), bottom_right_corner.y());
  glVertex2d(bottom_left_corner.x(), bottom_left_corner.y());
  glEnd();
  glBegin(GL_LINES);
  glVertex2d(bottom_left_corner.x(), bottom_left_corner.y());
  glVertex2d(top_left_corner.x(), top_left_corner.y());
  glEnd();
}

void Plotter::updateMap(lanelet::LaneletMapConstPtr map) { lanelet_map_ = map; }

void Plotter::updateIntersections(
    const std::vector<Intersection>& intersections) {
  intersections_ = intersections;
}

void Plotter::keyboardCB(unsigned char key, int x, int y) {
  switch (key) {
    case 27:
      glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE,
                    GLUT_ACTION_CONTINUE_EXECUTION);
      glutLeaveMainLoop();
      break;

    default:
      break;
  }

  glutPostRedisplay();
}

void mouseMotionCB(int x, int y) {
  if (mouseRightDown) {
    double screenX = 4 / sqrt(3) * cameraDistance;
    double screenY = screenX / 2;
    double ptx = screenX / SIZE_X * x - screenX / 2 + cameraX;
    double pty = screenY / 2 - screenY / SIZE_Y * y + cameraY;
    cameraX -= (ptx - mouseX) * 0.7f;
    cameraY -= (pty - mouseY) * 0.7f;
    mouseX = ptx;
    mouseY = pty;
    setCamera();
  }
}

void mouseCB(int button, int state, int x, int y) {
  if (button == GLUT_LEFT_BUTTON) {
    if (state == GLUT_DOWN) {
      double screenX = 4 / sqrt(3) * cameraDistance;
      double screenY = screenX / 2;
      double ptx = screenX / SIZE_X * x - screenX / 2 + cameraX;
      double pty = screenY / 2 - screenY / SIZE_Y * y + cameraY;
      cameraX = ptx;
      cameraY = pty;
      glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT |
              GL_STENCIL_BUFFER_BIT);
      setCamera();
    }
  }
  if ((button == 3) || (button == 4)) {
    if (state == GLUT_UP) return;

    if (button == 3) {
      if (cameraDistance < 20)
        cameraDistance--;
      else
        cameraDistance -= 10;

      if (cameraDistance < 0) cameraDistance = 0;

    } else
      cameraDistance += 10;
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    setCamera();
  }
  if (button == GLUT_RIGHT_BUTTON) {
    if (state == GLUT_DOWN) {
      mouseRightDown = true;
      double screenX = 4 / sqrt(3) * cameraDistance;
      double screenY = screenX / 2;
      double ptx = screenX / SIZE_X * x - screenX / 2 + cameraX;
      double pty = screenY / 2 - screenY / SIZE_Y * y + cameraY;
      mouseX = ptx;
      mouseY = pty;
    } else if (state == GLUT_UP) {
      mouseRightDown = false;
    }
  }
}

void setCamera(float posX, float posY, float posZ, float targetX, float targetY,
               float targetZ, float upX, float upY, float upZ) {
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  gluLookAt(static_cast<double>(posX), static_cast<double>(posY),
            static_cast<double>(posZ), static_cast<double>(targetX),
            static_cast<double>(targetY), static_cast<double>(targetZ),
            static_cast<double>(upX), static_cast<double>(upY),
            static_cast<double>(upZ));
}

void setCamera() {
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  gluLookAt(static_cast<double>(cameraX), static_cast<double>(cameraY),
            static_cast<double>(cameraDistance), static_cast<double>(cameraX),
            static_cast<double>(cameraY), static_cast<double>(0), 0, 1, 0);
}
