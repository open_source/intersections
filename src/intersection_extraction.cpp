#include "intersection_extraction.h"

std::vector<Intersection> IntersectionExtraction::FindIntersections(
    const lanelet::LaneletMapPtr lanelet_map,
    const lanelet::routing::RoutingGraphPtr& lanelet_routing_graph) {
  auto& lanelet_layer = lanelet_map->laneletLayer;
  std::vector<std::vector<bool>> intersection_table(
      lanelet_layer.size(), std::vector<bool>(lanelet_layer.size()));

  // Find lanelets with more than one succesor and predecessor
  std::map<int, lanelet::ConstLanelet> interesting_lanelets;
  for (auto& lanelet : lanelet_layer) {
    // interesting_lanelets.emplace(lanelet.id(),lanelet);
    auto succesor_lanelets = lanelet_routing_graph->following(lanelet);
    if (succesor_lanelets.size() > 1) {
      for (auto& sucessor_lanelet : succesor_lanelets) {
        interesting_lanelets.emplace(sucessor_lanelet.id(), sucessor_lanelet);
      }
    }
    auto predecessor_lanelets = lanelet_routing_graph->previous(lanelet);
    if (predecessor_lanelets.size() > 1) {
      for (auto& predecessor_lanelet : predecessor_lanelets) {
        interesting_lanelets.emplace(predecessor_lanelet.id(),
                                     predecessor_lanelet);
      }
    }
  }

  // Create a table of lanelet intersections
  for (auto it = interesting_lanelets.begin(); it != interesting_lanelets.end();
       ++it) {
    for (auto other_it = interesting_lanelets.begin();
         other_it != interesting_lanelets.end(); ++other_it) {
      int i = std::distance(interesting_lanelets.begin(), it);
      int j = std::distance(interesting_lanelets.begin(), other_it);
      if (i == j) {
        continue;
      }
      intersection_table.at(i).at(j) = lanelet::geometry::overlaps2d(
          (*it).second.polygon2d(), (*other_it).second.polygon2d());
    }
  }

  // Do a recursive search in the table to group the lanelets into intersections
  std::vector<Intersection> intersections;
  for (int i = 0; i < interesting_lanelets.size(); i++) {
    std::map<int, lanelet::ConstLanelet> map_intersection,
        recursive_intersection;
    GroupLaneletsRecursive(i, interesting_lanelets, recursive_intersection,
                           intersection_table);

    // Erase the lanelets with the same predecessor or adjacent predecessors and
    // save the ones with same succesor
    for (auto it = recursive_intersection.begin();
         it != recursive_intersection.end(); ++it) {
      for (auto other_it = std::next(it);
           other_it != recursive_intersection.end(); ++other_it) {
        if (lanelet::geometry::overlaps2d((*it).second.polygon2d(),
                                          (*other_it).second.polygon2d())) {
          auto succesor_lanelets =
              lanelet_routing_graph->following((*it).second);
          auto other_succesor_lanelets =
              lanelet_routing_graph->following((*other_it).second);
          bool successor_found = false;
          for (auto& succesor_lanelet : succesor_lanelets) {
            for (auto& other_succesor_lanelet : other_succesor_lanelets) {
              if (succesor_lanelet.id() == other_succesor_lanelet.id()) {
                map_intersection.emplace((*it).first, (*it).second);
                map_intersection.emplace((*other_it).first, (*other_it).second);
                successor_found = true;
              }
            }
          }
          if (successor_found) {
            continue;
          }
          auto predecessor_lanelets =
              lanelet_routing_graph->previous((*it).second);
          auto other_predecessor_lanelets =
              lanelet_routing_graph->previous((*other_it).second);
          bool predecessor_found = false;
          for (auto& predecessor_lanelet : predecessor_lanelets) {
            for (auto& other_predecessor_lanelet : other_predecessor_lanelets) {
              auto routing_relation = lanelet_routing_graph->routingRelation(
                  predecessor_lanelet, other_predecessor_lanelet);
              bool adjacent = false;
              if (routing_relation) {
                auto relation = routing_relation.get();
                if (relation == lanelet::routing::RelationType::Left ||
                    relation == lanelet::routing::RelationType::Right) {
                  adjacent = true;
                }
              }
              if (predecessor_lanelet.id() == other_predecessor_lanelet.id() ||
                  adjacent) {
                predecessor_found = true;
              }
            }
          }
          if (!predecessor_found) {
            map_intersection.emplace((*it).first, (*it).second);
            map_intersection.emplace((*other_it).first, (*other_it).second);
          }
        }
      }
    }

    if (map_intersection.size() > 1) {
      Intersection intersection;
      intersection.lanelet_map.insert(map_intersection.begin(),
                                      map_intersection.end());

      for (auto it = map_intersection.begin(); it != map_intersection.end();
           ++it) {
        intersection.exits.push_back((*it).second.centerline2d().back());
        for (auto other_it = std::next(it); other_it != map_intersection.end();
             ++other_it) {
          if (lanelet::geometry::touches2d((*it).second.polygon2d(),
                                           (*other_it).second.polygon2d())) {
            continue;
          }
          lanelet::BasicPolygon2d in_polygon_1 =
              (*it).second.polygon2d().basicPolygon();
          lanelet::BasicPolygon2d in_polygon_2 =
              (*other_it).second.polygon2d().basicPolygon();
          lanelet::BasicPolygon2d out_polygon;
          boost::geometry::correct(in_polygon_1);
          boost::geometry::correct(in_polygon_2);
          boost::geometry::intersection(in_polygon_1, in_polygon_2,
                                        out_polygon);
          if (out_polygon.size() > 2) {
            intersection.intersection_polygons.push_back(out_polygon);
          }
        }
      }
      // Create Bounding Box for the intersection polygons
      for (auto& polygon : intersection.intersection_polygons) {
        lanelet::BoundingBox2d temp_bounding_box;
        temp_bounding_box = lanelet::geometry::boundingBox2d(polygon);
        temp_bounding_box.extend(intersection.bounding_box);
        intersection.bounding_box = temp_bounding_box;
      }

      // Search connection corridors
      for (auto& lanelet : intersection.lanelet_map) {
        auto previous_lanelets =
            lanelet_routing_graph->previous(lanelet.second);
        for (auto& previous_lanelet : previous_lanelets) {
          if (intersection.lanelet_map.find(previous_lanelet.id()) ==
              intersection.lanelet_map.end()) {
            std::unordered_map<int, lanelet::ConstLanelets>
                bifurcating_lanelets;
            std::unordered_map<int, bool> exits_found;
            int index = 0;
            lanelet::ConstLanelets lanelets;
            bifurcating_lanelets.emplace(index, lanelets);
            exits_found.emplace(index, false);
            SearchSuccesorRecursive(index, bifurcating_lanelets, exits_found,
                                    lanelet.second, lanelet.second,
                                    intersection, lanelet_routing_graph);
            for (auto& lanelets : bifurcating_lanelets) {
              if (lanelets.second.size() == 0) {
                continue;
              }
              ConnectionCorridor connection_corridor;
              connection_corridor.id = lanelet::utils::getId();
              connection_corridor.lanelets.insert(
                  connection_corridor.lanelets.end(), lanelets.second.begin(),
                  lanelets.second.end());
              intersection.connection_corridors.push_back(connection_corridor);
              break;
            }
          }
        }
      }

      // Search Priorities
      std::unordered_map<int, std::unordered_map<int, int>> priorities;
      for (auto& connection_corridor : intersection.connection_corridors) {
        for (auto& other_connection_corridor :
             intersection.connection_corridors) {
          priorities[connection_corridor.id][other_connection_corridor.id] = 0;
        }
      }

      for (auto& connection_corridor : intersection.connection_corridors) {
        for (auto& other_connection_corridor :
             intersection.connection_corridors) {
          if (CorridorsOverlap(connection_corridor, other_connection_corridor,
                               lanelet_routing_graph)) {
            priorities[connection_corridor.id][other_connection_corridor.id] =
                1;
          } else {
            priorities[connection_corridor.id][other_connection_corridor.id] =
                0;
          }
        }

        for (auto& lanelet : connection_corridor.lanelets) {
          std::vector<std::shared_ptr<const lanelet::RightOfWay>>
              right_of_way_elements_vector;
          auto lanelet_right_of_way =
              lanelet.regulatoryElementsAs<lanelet::RightOfWay>();
          right_of_way_elements_vector.insert(
              right_of_way_elements_vector.end(), lanelet_right_of_way.begin(),
              lanelet_right_of_way.end());
          auto previous_lanelets = lanelet_routing_graph->previous(lanelet);
          for (auto& previous_lanelet : previous_lanelets) {
            auto previous_lanelet_right_of_way =
                previous_lanelet.regulatoryElementsAs<lanelet::RightOfWay>();
            right_of_way_elements_vector.insert(
                right_of_way_elements_vector.end(),
                previous_lanelet_right_of_way.begin(),
                previous_lanelet_right_of_way.end());
          }
          for (auto& reg_elem : right_of_way_elements_vector) {
            auto prioritary_lanelets = reg_elem->rightOfWayLanelets();
            std::vector<int64_t> prioritary_corridors_ids,
                non_prioritary_corridors_ids;
            for (auto& prioritary_lanelet : prioritary_lanelets) {
              std::vector<int64_t> temp_prioritary_corridors_ids =
                  GetCorridorsWithLanelet(prioritary_lanelet,
                                          intersection.connection_corridors,
                                          lanelet_routing_graph);
              prioritary_corridors_ids.insert(
                  prioritary_corridors_ids.begin(),
                  temp_prioritary_corridors_ids.begin(),
                  temp_prioritary_corridors_ids.end());
            }
            auto non_prioritary_lanelets = reg_elem->yieldLanelets();
            for (auto& non_prioritary_lanelet : non_prioritary_lanelets) {
              std::vector<int64_t> temp_non_prioritary_corridors_ids =
                  GetCorridorsWithLanelet(non_prioritary_lanelet,
                                          intersection.connection_corridors,
                                          lanelet_routing_graph);
              non_prioritary_corridors_ids.insert(
                  non_prioritary_corridors_ids.begin(),
                  temp_non_prioritary_corridors_ids.begin(),
                  temp_non_prioritary_corridors_ids.end());
            }
            for (auto& prioritary_id : prioritary_corridors_ids) {
              for (auto& non_prioritary_id : non_prioritary_corridors_ids) {
                priorities.at(prioritary_id).at(non_prioritary_id) = 2;
              }
            }
          }
        }

        lanelet::LaneletSequence lanelet_sequence(connection_corridor.lanelets);
        float signed_area = SignedArea(lanelet_sequence);
        for (auto& other_connection_corridor :
             intersection.connection_corridors) {
          if ((priorities.at(connection_corridor.id)
                   .at(other_connection_corridor.id) == 1) &&
              (priorities.at(other_connection_corridor.id)
                   .at(connection_corridor.id) == 1)) {
            lanelet::LaneletSequence other_lanelet_sequence(
                other_connection_corridor.lanelets);
            float other_signed_area = SignedArea(other_lanelet_sequence);
            if (((signed_area < 0.1 && signed_area > -0.1) ||
                 signed_area < -0.1) &&
                other_signed_area > 0.1) {
              priorities[connection_corridor.id][other_connection_corridor.id] =
                  2;
            }
          }
        }

        // Correct the entrances to the intersection
        auto& bounding_box = intersection.bounding_box;
        auto top_left_corner =
            bounding_box.corner(lanelet::BoundingBox2d::TopLeft);
        auto top_right_corner =
            bounding_box.corner(lanelet::BoundingBox2d::TopRight);
        auto bottom_right_corner =
            bounding_box.corner(lanelet::BoundingBox2d::BottomRight);
        auto bottom_left_corner =
            bounding_box.corner(lanelet::BoundingBox2d::BottomLeft);
        lanelet::Point3d top_left_point{lanelet::utils::getId(),
                                        top_left_corner.x(),
                                        top_left_corner.y(), 0.0};
        lanelet::Point3d top_right_point{lanelet::utils::getId(),
                                         top_right_corner.x(),
                                         top_right_corner.y(), 0.0};
        lanelet::Point3d bottom_right_point{lanelet::utils::getId(),
                                            bottom_right_corner.x(),
                                            bottom_right_corner.y(), 0.0};
        lanelet::Point3d bottom_left_point{lanelet::utils::getId(),
                                           bottom_left_corner.x(),
                                           bottom_left_corner.y(), 0.0};
        lanelet::Polygon2d bounding_box_polygon(
            lanelet::utils::getId(), {top_left_point, top_right_point,
                                      bottom_right_point, bottom_left_point});

        for (auto& lanelet : intersection.lanelet_map) {
          std::vector<std::shared_ptr<const lanelet::RightOfWay>>
              right_of_way_elements_vector;
          auto lanelet_right_of_way =
              lanelet.second.regulatoryElementsAs<lanelet::RightOfWay>();
          right_of_way_elements_vector.insert(
              right_of_way_elements_vector.end(), lanelet_right_of_way.begin(),
              lanelet_right_of_way.end());
          auto previous_lanelets =
              lanelet_routing_graph->previous(lanelet.second);
          for (auto& previous_lanelet : previous_lanelets) {
            auto previous_lanelet_right_of_way =
                previous_lanelet.regulatoryElementsAs<lanelet::RightOfWay>();
            right_of_way_elements_vector.insert(
                right_of_way_elements_vector.end(),
                previous_lanelet_right_of_way.begin(),
                previous_lanelet_right_of_way.end());
          }
          bool reg_elem_intersected = false;
          for (auto& reg_elem : right_of_way_elements_vector) {
            lanelet::BasicPolygon3d out_polygon;
            boost::geometry::intersection(
                lanelet.second.centerline3d().basicLineString(),
                reg_elem->stopLine()->basicLineString(), out_polygon);
            if (out_polygon.size() != 0) {
              intersection.entrances.push_back(lanelet::BasicPoint2d(
                  out_polygon.front().x(), out_polygon.front().y()));
              reg_elem_intersected = true;
            }
          }
          if (!reg_elem_intersected) {
            lanelet::BasicPolygon2d out_polygon;
            boost::geometry::intersection(
                lanelet.second.centerline2d().basicLineString(),
                bounding_box_polygon.basicPolygon(), out_polygon);

            if (out_polygon.size() != 0) {
              intersection.entrances.push_back(out_polygon.front());
            } else {
              intersection.entrances.push_back(
                  lanelet.second.centerline2d().front());
            }
          }
        }
      }

      intersection.id = lanelet::utils::getId();
      lanelet::utils::registerId(intersection.id);
      intersections.push_back(intersection);
    }
  }

  return intersections;
}

void IntersectionExtraction::GroupLaneletsRecursive(
    int index, std::map<int, lanelet::ConstLanelet>& interesting_lanelets,
    std::map<int, lanelet::ConstLanelet>& intersection,
    std::vector<std::vector<bool>>& intersection_table) {
  std::vector<int> intersection_positives;
  for (int i = 0; i < intersection_table.at(index).size(); ++i) {
    if (intersection_table.at(index).at(i)) {
      intersection_positives.push_back(i);
    }
  }

  for (int j = 0; j < intersection_positives.size(); j++) {
    intersection_table.at(index).at(intersection_positives.at(j)) = false;
    intersection.emplace(
        (*std::next(interesting_lanelets.begin(), index)).first,
        (*std::next(interesting_lanelets.begin(), index)).second);
    intersection.emplace(
        (*std::next(interesting_lanelets.begin(), intersection_positives.at(j)))
            .first,
        (*std::next(interesting_lanelets.begin(), intersection_positives.at(j)))
            .second);
    IntersectionExtraction::GroupLaneletsRecursive(
        intersection_positives.at(j), interesting_lanelets, intersection,
        intersection_table);
  }
}

void IntersectionExtraction::SearchSuccesorRecursive(
    int& index,
    std::unordered_map<int, lanelet::ConstLanelets>& bifurcating_lanelets,
    std::unordered_map<int, bool>& exits_found,
    lanelet::ConstLanelet& current_lanelet,
    lanelet::ConstLanelet& previous_lanelet, Intersection& intersection,
    const lanelet::routing::RoutingGraphPtr& lanelet_routing_graph) {
  lanelet::ConstLanelets next_lanelets;
  if (bifurcating_lanelets.count(index) != 1) {
    for (auto& lanelet : bifurcating_lanelets[index - 1]) {
      bifurcating_lanelets[index].push_back(lanelet);
      if (lanelet.id() == previous_lanelet.id()) {
        break;
      }
    }
  }
  bifurcating_lanelets[index].push_back(current_lanelet);
  auto succesor_lanelets = lanelet_routing_graph->following(current_lanelet);
  for (auto& succesor_lanelet : succesor_lanelets) {
    if (intersection.lanelet_map.find(succesor_lanelet.id()) ==
        intersection.lanelet_map.end()) {
      exits_found[index] = true;
      break;
    }
  }
  while (!exits_found[index]) {
    next_lanelets = lanelet_routing_graph->following(current_lanelet);
    if (next_lanelets.empty()) {
      break;
    }
    for (int i = 0; i < next_lanelets.size(); i++) {
      index += i;
      SearchSuccesorRecursive(index, bifurcating_lanelets, exits_found,
                              next_lanelets.at(i), current_lanelet,
                              intersection, lanelet_routing_graph);
    }
  }
}

bool IntersectionExtraction::CorridorsOverlap(
    const ConnectionCorridor& corridor,
    const ConnectionCorridor& other_corridor,
    const lanelet::routing::RoutingGraphPtr& lanelet_routing_graph) {
  for (auto& lanelet : corridor.lanelets) {
    auto previous_lanelets = lanelet_routing_graph->previous(lanelet);
    for (auto& other_lanelet : other_corridor.lanelets) {
      auto other_previous_lanelets =
          lanelet_routing_graph->previous(other_lanelet);
      for (auto& previous_lanelet : previous_lanelets) {
        for (auto& other_previous_lanelet : other_previous_lanelets) {
          if (previous_lanelet.id() == other_previous_lanelet.id()) {
            return false;
          }
        }
      }
      if (lanelet::geometry::overlaps2d(lanelet.polygon2d(),
                                        other_lanelet.polygon2d())) {
        return true;
      }
    }
  }
  return false;
}

float IntersectionExtraction::SignedArea(
    const lanelet::LaneletSequence& lanelet_sequence) {
  float signed_area = 0;
  for (auto it = lanelet_sequence.centerline2d().begin();
       it < lanelet_sequence.centerline2d().end(); it++) {
    auto& point = (*it);
    lanelet::ConstPoint2d next_point;
    if (it == lanelet_sequence.centerline2d().end() - 1) {
      next_point = *(lanelet_sequence.centerline2d().begin());
    } else {
      next_point = *(it + 1);
    }
    signed_area = signed_area +
                  ((point.x() * next_point.y()) - (next_point.x() * point.y()));
  }
  return signed_area / 2;
}

std::vector<int64_t> IntersectionExtraction::GetCorridorsWithLanelet(
    lanelet::ConstLanelet& target_lanelet,
    const std::vector<ConnectionCorridor>& connection_corridors,
    const lanelet::routing::RoutingGraphPtr& lanelet_routing_graph) {
  std::vector<int64_t> returned_corridors;

  auto following_target_lanelets =
      lanelet_routing_graph->following(target_lanelet);
  for (auto& connection_corridor : connection_corridors) {
    for (auto& lanelet : connection_corridor.lanelets) {
      if (lanelet.id() == target_lanelet.id()) {
        returned_corridors.push_back(connection_corridor.id);
        break;
      }
      bool broke = false;
      for (auto& following_target_lanelet : following_target_lanelets) {
        if (lanelet.id() == following_target_lanelet.id()) {
          returned_corridors.push_back(connection_corridor.id);
          broke = true;
          break;
        }
      }
      if (broke) break;
    }
  }
  return returned_corridors;
}
