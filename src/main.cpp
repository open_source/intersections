#include <lanelet2_io/Io.h>
#include <lanelet2_projection/UTM.h>
#include <lanelet2_traffic_rules/TrafficRulesFactory.h>
#include <iostream>
#include "intersection_extraction.h"
#include "plot.h"

int main(int argc, char* argv[]) {
  std::ifstream file("../maps.csv");
  std::vector<std::string> folders, map_names, origins_lat, origins_long;
  std::string folder, map_name, origin_lat, origin_long;
  int id = 0;
  while (getline(file, folder, ',')) {
    getline(file, map_name, ',');
    getline(file, origin_lat, ',');
    getline(file, origin_long);
    std::cout << "ID: " << id << " ";
    std::cout << "Dataset: " << folder << " ";
    std::cout << "Map Name: " << map_name << std::endl;
    folders.push_back(folder);
    map_names.push_back(map_name);
    origins_lat.push_back(origin_lat);
    origins_long.push_back(origin_long);
    id++;
  }

  std::cout << "Please select map ID" << std::endl;
  std::string input_id;
  char ds = ' ';
  bool valid_input = false;
  while (!valid_input) {
    while (ds != '\n') {
      std::cin >> input_id;
      std::cin.get(ds);
    }
    if (std::stoi(input_id) < folders.size()) {
      valid_input = true;
    } else {
      std::cout << "Invalid input, please introduce a number between 0 and "
                << id - 1 << std::endl;
      input_id.clear();
      std::cin.clear();
      ds = ' ';
    }
  }
  std::cout << "Selected map ID " << input_id << std::endl;

  std::string map_file_path = "../test_maps/" +
                              folders.at(std::stoi(input_id)) + "/" +
                              map_names.at(std::stoi(input_id));
  double origin_latitude = std::stod(origins_lat.at(std::stoi(input_id)));
  double origin_longitude = std::stod(origins_long.at(std::stoi(input_id)));

  lanelet::projection::UtmProjector projector(
      lanelet::Origin({origin_latitude, origin_longitude}));
  lanelet::LaneletMapPtr lanelet_map = lanelet::load(map_file_path, projector);
  lanelet::traffic_rules::TrafficRulesPtr traffic_rules =
      lanelet::traffic_rules::TrafficRulesFactory::create(
          lanelet::Locations::Germany, lanelet::Participants::Vehicle);
  lanelet::routing::RoutingGraphPtr lanelet_routing_graph =
      lanelet::routing::RoutingGraph::build(*lanelet_map, *traffic_rules);

  std::vector<Intersection> intersections =
      IntersectionExtraction::FindIntersections(lanelet_map,
                                                lanelet_routing_graph);

  if (intersections.size() > 0) {
    std::cout << "--Intersections Extracted--" << std::endl;
    std::cout << "Number of Intersections: " << intersections.size()
              << std::endl;
  } else {
    std::cout << "--No Intersections Extracted--" << std::endl;
    return 0;
  }

  Plotter* plotter = new Plotter(argc, argv);
  plotter->updateMap(lanelet_map);
  plotter->updateIntersections(intersections);
  plotter->initPlot();
  glutMainLoop();

  return 0;
}
