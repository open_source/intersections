# Intersection Detection
> Algorithm to detect intersections based on Lanelet2 and Open-Street Maps

![](intersection.png)

## Compilation

Compile the program:

```sh
mkdir build
cd build
cmake ..
make
```

## Usage example

```sh
cd build
./intersections
```

